package yp.demo.officeconvert.representation

import com.fasterxml.jackson.databind.ObjectMapper
import org.spockframework.spring.SpringBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import yp.demo.officeconvert.application.ApplicationConvertService
import yp.demo.officeconvert.application.vo.ConvertRequest
import yp.demo.officeconvert.domain.model.Word
import yp.demo.officeconvert.domain.service.DomainConvertService

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@SpringBootTest
@AutoConfigureMockMvc
class ConvertControllerTest extends Specification {
    @Autowired
    private MockMvc mvc

    @Autowired
    ApplicationConvertService convertService

    @SpringBean
    DomainConvertService domainConvertService = Mock(DomainConvertService)

    def "when post for convert a word file then the response has status 200 and content is null"() {
        given:
        domainConvertService.convertOfficeToPdf(_) >> "testPdf.pdf"
        def wordConvertRequest = new ConvertRequest(fileDownloadUrl: 'testDownloadUrl')

        when:
        ObjectMapper objectMapper = new ObjectMapper()
        def convertResults = mvc.perform(post('/word')
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(wordConvertRequest)))

        then: "Status is 200 and the response is target file path"
        convertResults.andExpect(status().isOk())
                .andReturn()
                .response
                .contentAsString == "{\"fileDownloadUrl\":\"testPdf.pdf\"}"
    }

    def "when post for convert a word file then the response has status 500 and content is error msg"() {
        given:
        domainConvertService.convertOfficeToPdf(_) >> {throw new RuntimeException("convert file error!")}
        def wordConvertRequest = new ConvertRequest(fileDownloadUrl: 'testDownloadUrl')

        when:
        ObjectMapper objectMapper = new ObjectMapper()
        def convertResults = mvc.perform(post('/word')
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(wordConvertRequest)))

        then: "Status is 200 and the response is target file path"
        convertResults.andExpect(status().isOk())
                .andReturn()
                .response
                .contentAsString == "{\"error\":\"convert file error!\"}"
    }

}
