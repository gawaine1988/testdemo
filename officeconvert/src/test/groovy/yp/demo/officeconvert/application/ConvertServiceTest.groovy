package yp.demo.officeconvert.application

import spock.lang.Specification
import yp.demo.officeconvert.application.vo.ConvertRequest
import yp.demo.officeconvert.domain.model.Word
import yp.demo.officeconvert.domain.resoure.WebResource
import yp.demo.officeconvert.domain.service.DomainConvertService

class ConvertServiceTest extends Specification {

    def webResource = Mock(WebResource)

    def setup() {
        webResource.downLoadFile("testDownloadUrl") >> "testWordFile.doc"
    }

    def "should return converted file url when convert a word file successfully"() {

        given: 'prepare request data'
        def domainConvertService = Mock(DomainConvertService)
        domainConvertService.convertOfficeToPdf(_) >> "testTargetPdf.pdf"

        def convertService = new ApplicationConvertService(domainConvertService, webResource)
        def convertRequest = new ConvertRequest(fileDownloadUrl: 'testDownloadUrl')

        when: 'convert word file'
        def convertResult = convertService.convertWord(convertRequest)

        then: 'return converted file url'
        convertResult.getFileDownloadUrl() == 'testTargetPdf.pdf'

    }

    def "should throw file not found exception when word download url is error"() {
        given: 'prepare request data'
        def domainConvertService = Mock(DomainConvertService)
        domainConvertService.convertOfficeToPdf(_ as Word) >> {
            throw new RuntimeException("Convert word to pdf error!")
        }

        def convertService = new ApplicationConvertService(domainConvertService, webResource)
        def convertRequest = new ConvertRequest(fileDownloadUrl: 'errorDownloadUrl')

        when: 'convert word file'
        convertService.convertWord(convertRequest)

        then: 'throw download error'
        RuntimeException exception = thrown()
        exception.message == "Convert word to pdf error!"

    }

}
