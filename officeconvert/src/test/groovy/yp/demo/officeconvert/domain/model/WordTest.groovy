package yp.demo.officeconvert.domain.model

import spock.lang.Specification
import yp.demo.officeconvert.domain.resoure.WebResource

class WordTest extends Specification {
    def "should download word file when create word"() {
        given: "prepare the webResource"
        def webReource = Mock(WebResource)
        def testDownloadPath = "testDownloadPath"
        def testLocalFilePath = "testLocalPath"
        webReource.downLoadFile(testDownloadPath) >> testLocalFilePath

        when: "create a new word"
        def word = Word.createWord(testDownloadPath,webReource)

        then: "should download word file"
        word.getLocalPath() == testLocalFilePath
    }
}
