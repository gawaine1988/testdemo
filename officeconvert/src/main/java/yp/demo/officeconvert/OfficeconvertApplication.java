package yp.demo.officeconvert;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OfficeconvertApplication {

    public static void main(String[] args) {
        SpringApplication.run(OfficeconvertApplication.class, args);
    }

}
