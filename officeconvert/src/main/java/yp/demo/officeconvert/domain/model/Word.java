package yp.demo.officeconvert.domain.model;

import lombok.Getter;
import lombok.Setter;
import yp.demo.officeconvert.domain.resoure.WebResource;

@Getter
@Setter
public class Word {
    private String localPath;

    private Word() {
    }

    public static Word createWord(String fileDownloadUrl, WebResource webResource) {
        String downloadedFilePath = webResource.downLoadFile(fileDownloadUrl);
        Word word = new Word();
        word.setLocalPath(downloadedFilePath);
        return word;
    }
}
