package yp.demo.officeconvert.domain.service;

import com.google.common.io.Files;
import org.jodconverter.OnlineConverter;
import org.jodconverter.document.DefaultDocumentFormatRegistry;
import org.jodconverter.office.OfficeException;
import org.jodconverter.office.OnlineOfficeManager;
import org.springframework.stereotype.Service;
import yp.demo.officeconvert.domain.model.Word;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class DomainConvertService {

    public String convertOfficeToPdf(Word word) {
        OnlineOfficeManager make = OnlineOfficeManager.make(word.getLocalPath());
        try {
            make.start();
            OnlineConverter build = OnlineConverter
                    .builder()
                    .officeManager(make)
                    .formatRegistry(DefaultDocumentFormatRegistry.getInstance())
                    .build();


            File localOfficeFile = new File(word.getLocalPath());
            File targetFile = new File(getTargetPdfPath(word.getLocalPath()));
            build.convert(localOfficeFile)
                    .to(targetFile)
                    .execute();
            make.stop();
            return "testTargetPdfPath";
        } catch (OfficeException e) {
            throw new RuntimeException("Convert error!");
        }
    }

    private String getTargetPdfPath(String sourceFileName) {
        Path path = Paths.get("./", Files.getNameWithoutExtension(sourceFileName));
        return path.toString() + ".pdf";
    }
}
