package yp.demo.officeconvert.application;

import org.springframework.stereotype.Service;
import yp.demo.officeconvert.application.vo.ConvertRequest;
import yp.demo.officeconvert.application.vo.ConvertResult;
import yp.demo.officeconvert.domain.service.DomainConvertService;
import yp.demo.officeconvert.domain.model.Word;
import yp.demo.officeconvert.domain.resoure.WebResource;

@Service
public class ApplicationConvertService {

    private final DomainConvertService domainConvertService;
    private final WebResource webResource;

    public ApplicationConvertService(DomainConvertService domainConvertService, WebResource webResource) {
        this.domainConvertService = domainConvertService;
        this.webResource = webResource;
    }

    public ConvertResult convertWord(ConvertRequest convertRequest) {

        Word word = Word.createWord(convertRequest.getFileDownloadUrl(), webResource);

        String convertedPdfPath = domainConvertService.convertOfficeToPdf(word);

        return ConvertResult.builder().fileDownloadUrl(convertedPdfPath).build();
    }

}
