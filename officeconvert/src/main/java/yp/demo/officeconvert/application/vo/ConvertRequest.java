package yp.demo.officeconvert.application.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConvertRequest {
    private String fileDownloadUrl;
}
