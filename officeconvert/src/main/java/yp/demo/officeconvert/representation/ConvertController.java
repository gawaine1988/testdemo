package yp.demo.officeconvert.representation;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import yp.demo.officeconvert.application.ApplicationConvertService;
import yp.demo.officeconvert.application.vo.ConvertRequest;
import yp.demo.officeconvert.application.vo.ConvertResult;

@RestController
public class ConvertController {
    private final ApplicationConvertService convertService;

    public ConvertController(ApplicationConvertService convertService) {
        this.convertService = convertService;
    }

    @PostMapping("/word")
    public ConvertResult convertWord(ConvertRequest convertRequest) {
        ConvertResult wordConvertResult = convertService.convertWord(convertRequest);
        return wordConvertResult;
    }
}
