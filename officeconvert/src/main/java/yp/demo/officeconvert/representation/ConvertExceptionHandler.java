package yp.demo.officeconvert.representation;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import yp.demo.officeconvert.application.vo.ConvertResult;

@ControllerAdvice
public class ConvertExceptionHandler {

    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    ConvertResult handleConflict(RuntimeException e) {
        ConvertResult errorResult = ConvertResult.builder().error(e.getLocalizedMessage()).build();

        return errorResult;
    }
}
